package ru.tomsksoft.testlentanews;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DisplayerActivity extends AppCompatActivity {

    ImageView imageItem;
    TextView titleItem, descriptionItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_displayer);

        imageItem = findViewById(R.id.image_item);
        titleItem = findViewById(R.id.title_item);
        descriptionItem = findViewById(R.id.description_item);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            titleItem.setText(bundle.get("Title").toString());
            descriptionItem.setText(bundle.get("Description").toString());
            Picasso.with(getApplicationContext())
                   .load(bundle.get("Image URL").toString())
                   .into(imageItem);
        }
    }
}
