package ru.tomsksoft.testlentanews;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;

import ru.tomsksoft.testlentanews.recycler.adapters.CompositeDelegateAdapter;
import ru.tomsksoft.testlentanews.recycler.adapters.TextDelegateAdapter;
import ru.tomsksoft.testlentanews.recycler.models.IViewModel;
import ru.tomsksoft.testlentanews.recycler.models.TextViewModel;

public class NewsListActivity extends AppCompatActivity {

    private CompositeDelegateAdapter mAdapterNewsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;

        final TextView titleItem = findViewById(R.id.list_news_title);
        titleItem.setText(bundle.get("Title").toString());

        final RecyclerView newsListRecyclerView = findViewById(R.id.list_recycler_view);
        newsListRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mAdapterNewsList = new CompositeDelegateAdapter.Builder<IViewModel>()
                .add(new TextDelegateAdapter(
                        new TextDelegateAdapter.Listener() {
                            @Override
                            public void onModelClicked(final TextViewModel model) {
                                Intent intent =
                                        new Intent(getApplicationContext(),
                                                   DisplayerActivity.class);
                                intent.putExtra("Title", model.title);
                                intent.putExtra("Description", model.description);
                                intent.putExtra("Image URL", model.imageUrl);
                                startActivity(intent);
                            }
                        }))
                .build();
        newsListRecyclerView.setAdapter(mAdapterNewsList);

        new DownloadLink().execute(bundle.get("URL").toString());
    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadLink extends AsyncTask<String, Void, ArrayList<RssItem>> {

        private String mUrlLink;

        @Override
        protected ArrayList<RssItem> doInBackground(final String... strings) {
            mUrlLink = strings[0];

            return RssItem.getRssItems(mUrlLink);
        }

        @Override
        protected void onPostExecute(final ArrayList<RssItem> rssItems) {
            super.onPostExecute(rssItems);

            ArrayList<IViewModel> rssTypeItems = new ArrayList<>();

            for (RssItem item : rssItems) {
                TextViewModel textViewModel = new TextViewModel(item.getTitle(),
                                                                item.getDescription(),
                                                                item.getImageURL());
                rssTypeItems.add(textViewModel);
            }
            mAdapterNewsList.swapData(rssTypeItems);
        }
    }
}
