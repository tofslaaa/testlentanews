package ru.tomsksoft.testlentanews.recycler.models;

import android.content.Context;
import android.support.annotation.NonNull;

public class TitleViewModel implements IViewModel {

    @NonNull public final String title;
    @NonNull public final String  imageUrl;

    public TitleViewModel(@NonNull final String title,
                          @NonNull final String imageUrl) {
        this.title = title;
        this.imageUrl = imageUrl;
    }
}
