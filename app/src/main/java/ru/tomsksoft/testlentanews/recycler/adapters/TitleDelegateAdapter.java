package ru.tomsksoft.testlentanews.recycler.adapters;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import ru.tomsksoft.testlentanews.R;
import ru.tomsksoft.testlentanews.recycler.base.BaseDelegateAdapter;
import ru.tomsksoft.testlentanews.recycler.base.BaseViewHolder;
import ru.tomsksoft.testlentanews.recycler.models.TitleViewModel;


public class TitleDelegateAdapter extends BaseDelegateAdapter<TitleDelegateAdapter
        .TitleViewHolder, TitleViewModel> {

    private Listener mClickListener;

    public TitleDelegateAdapter(Listener clickListener) {
        this.mClickListener = clickListener;
    }

    @Override
    protected void onBindViewHolder(@NonNull final View view,
                                    @NonNull final TitleViewModel item,
                                    @NonNull final TitleViewHolder viewHolder) {
        viewHolder.title.setText(item.title);
        viewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mClickListener.onModelClicked(item);
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.title_type_item;
    }

    @NonNull
    @Override
    protected TitleViewHolder createViewHolder(final View parent) {
        return new TitleViewHolder(parent);
    }

    @Override
    public boolean isForViewType(@NonNull final List<?> items, final int position) {
        return items.get(position) instanceof TitleViewModel;
    }

    final static class TitleViewHolder extends BaseViewHolder {

        private TextView title;
        private Button button;

        public TitleViewHolder(@NonNull final View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            button = itemView.findViewById(R.id.view_list_btn);
        }
    }

    public interface Listener {
        void onModelClicked(TitleViewModel model);
    }
}
