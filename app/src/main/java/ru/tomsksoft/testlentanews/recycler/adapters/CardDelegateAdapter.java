package ru.tomsksoft.testlentanews.recycler.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.tomsksoft.testlentanews.MainActivity;
import ru.tomsksoft.testlentanews.R;
import ru.tomsksoft.testlentanews.recycler.base.BaseDelegateAdapter;
import ru.tomsksoft.testlentanews.recycler.base.BaseViewHolder;
import ru.tomsksoft.testlentanews.recycler.models.CardViewModel;

public class CardDelegateAdapter extends BaseDelegateAdapter<CardDelegateAdapter.CardViewHolder, CardViewModel> {

    private Listener mClickListener;

    public CardDelegateAdapter(final Listener clickListener) {
        mClickListener = clickListener;
    }

    @Override
    protected void onBindViewHolder(@NonNull final View view,
                                    @NonNull final CardViewModel item,
                                    @NonNull final CardViewHolder viewHolder) {
        Picasso.with(item.context).load(item.imageUrl).into(viewHolder.image);
        viewHolder.title.setText(item.title);
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mClickListener.onModelClicked(item);
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.card_type_item;
    }

    @NonNull
    @Override
    protected CardViewHolder createViewHolder(final View parent) {
        return new CardViewHolder(parent);
    }

    @Override
    public boolean isForViewType(@NonNull final List<?> items, final int position) {
        return items.get(position) instanceof CardViewModel;
    }

    final static class CardViewHolder extends BaseViewHolder {

        private CardView cardView;
        private ImageView image;
        private TextView title;

        public CardViewHolder(@NonNull final View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            image = itemView.findViewById(R.id.card_image_item);
            title = itemView.findViewById(R.id.card_news_title);
        }
    }

    public interface Listener {
        void onModelClicked(CardViewModel model);
    }
}
