package ru.tomsksoft.testlentanews.recycler.models;

import android.content.Context;
import android.support.annotation.NonNull;

public class CardViewModel implements IViewModel {

    @NonNull
    public final String title;
    @NonNull
    public final String description;
    @NonNull
    public final String imageUrl;
    @NonNull
    public final Context context;

    public CardViewModel(@NonNull final String title,
                         @NonNull final String description,
                         @NonNull final String imageUrl, @NonNull final Context context) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.context = context;
    }
}
