package ru.tomsksoft.testlentanews.recycler.adapters;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import ru.tomsksoft.testlentanews.MainActivity;
import ru.tomsksoft.testlentanews.R;
import ru.tomsksoft.testlentanews.recycler.base.BaseDelegateAdapter;
import ru.tomsksoft.testlentanews.recycler.base.BaseViewHolder;
import ru.tomsksoft.testlentanews.recycler.models.TextViewModel;
import ru.tomsksoft.testlentanews.recycler.models.TitleViewModel;

public class TextDelegateAdapter extends BaseDelegateAdapter<TextDelegateAdapter.TextViewHolder,
        TextViewModel> {

    private Listener mClickListener;

    public TextDelegateAdapter(final Listener clickListener) {
        mClickListener = clickListener;
    }

    @Override
    protected void onBindViewHolder(@NonNull final View view,
                                    @NonNull final TextViewModel item,
                                    @NonNull final TextViewHolder viewHolder) {
        viewHolder.title.setText(item.title);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mClickListener.onModelClicked(item);
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.text_type_item;
    }

    @NonNull
    @Override
    protected TextViewHolder createViewHolder(final View parent) {
        return new TextViewHolder(parent);
    }

    @Override
    public boolean isForViewType(@NonNull final List<?> items, final int position) {
        return items.get(position) instanceof TextViewModel;
    }

    final static class TextViewHolder extends BaseViewHolder {

        private TextView title;

        public TextViewHolder(@NonNull final View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.news_title);
        }
    }

    public interface Listener {
        void onModelClicked(TextViewModel model);
    }
}
