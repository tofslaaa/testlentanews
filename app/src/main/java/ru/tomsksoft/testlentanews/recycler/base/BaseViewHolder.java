package ru.tomsksoft.testlentanews.recycler.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class BaseViewHolder extends RecyclerView.ViewHolder {

    private ItemInflateListener listener;

    public BaseViewHolder(@NonNull final View itemView) {
        super(itemView);
    }

    public final void setListener(ItemInflateListener listener) {
        this.listener = listener;
    }

    public final void bind(Object item) {
        listener.inflated(item, itemView);
    }

    interface ItemInflateListener {
        void inflated(Object viewType, View view);
    }
}
