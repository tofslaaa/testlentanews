package ru.tomsksoft.testlentanews.recycler.models;

import android.support.annotation.NonNull;

public class TextViewModel implements IViewModel {

    @NonNull
    public final String title;
    @NonNull
    public final String description;
    @NonNull
    public final String imageUrl;

    public TextViewModel(@NonNull final String title,
                         @NonNull final String description,
                         @NonNull final String imageUrl) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
    }
}
