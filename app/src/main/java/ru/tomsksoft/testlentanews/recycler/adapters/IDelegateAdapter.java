package ru.tomsksoft.testlentanews.recycler.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

public interface IDelegateAdapter<VH extends RecyclerView.ViewHolder, T> {

    @NonNull
    RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType);

    void onBindViewHolder(@NonNull VH holder,
                          @NonNull List<T> items,
                          int position);

    void onRecycled(VH holder);

    boolean isForViewType(@NonNull List<?> items, int position);
}
