package ru.tomsksoft.testlentanews;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.ArrayList;

import ru.tomsksoft.testlentanews.recycler.adapters.CardDelegateAdapter;
import ru.tomsksoft.testlentanews.recycler.adapters.CompositeDelegateAdapter;
import ru.tomsksoft.testlentanews.recycler.adapters.TextDelegateAdapter;
import ru.tomsksoft.testlentanews.recycler.adapters.TitleDelegateAdapter;
import ru.tomsksoft.testlentanews.recycler.models.CardViewModel;
import ru.tomsksoft.testlentanews.recycler.models.IViewModel;
import ru.tomsksoft.testlentanews.recycler.models.TextViewModel;
import ru.tomsksoft.testlentanews.recycler.models.TitleViewModel;

public class MainActivity extends AppCompatActivity {

    private CompositeDelegateAdapter adapter;

    private final ArrayList<RssItem> rssItemTop7 = new ArrayList<>();
    private final ArrayList<RssItem> rssItemLast24 = new ArrayList<>();
    private final ArrayList<RssItem> rssItemAll = new ArrayList<>();

    private static final String URL_TOP7 = "https://lenta.ru/rss/top7";
    private static final String URL_LAST24 = "https://lenta.ru/rss/last24";
    private static final String URL_ALL = "https://lenta.ru/rss/news";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializedRecycler();

        new DownloadLink().execute();
    }

    void initializedRecycler() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        final GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        layoutManager.setSpanSizeLookup(
                new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(final int i) {
                        switch (adapter.getItemViewType(i)) {
                            case CompositeDelegateAdapter.TITLE_TYPE_VIEW:
                                return 2;
                            case CompositeDelegateAdapter.TEXT_TYPE_VIEW:
                                return 2;
                            case CompositeDelegateAdapter.CARD_TYPE_VIEW:
                                return 1;
                            default:
                                return 2;
                        }
                    }
                });
        recyclerView.setLayoutManager(layoutManager);

        adapter = new CompositeDelegateAdapter.Builder<IViewModel>()
                .add(new TitleDelegateAdapter(new TitleDelegateAdapter.Listener() {
                    @Override
                    public void onModelClicked(final TitleViewModel model) {
                        Intent intent = new Intent(getApplicationContext(), NewsListActivity.class);
                        intent.putExtra("URL", model.imageUrl);
                        intent.putExtra("Title", model.title);
                        startActivity(intent);
                    }
                }))
                .add(new TextDelegateAdapter(new TextDelegateAdapter.Listener() {
                    @Override
                    public void onModelClicked(final TextViewModel model) {
                        Intent intent =
                                new Intent(getApplicationContext(), DisplayerActivity.class);
                        intent.putExtra("Title", model.title);
                        intent.putExtra("Description", model.description);
                        intent.putExtra("Image URL", model.imageUrl);
                        startActivity(intent);
                    }
                }))
                .add(new CardDelegateAdapter(new CardDelegateAdapter.Listener() {
                    @Override
                    public void onModelClicked(final CardViewModel model) {
                        Intent intent =
                                new Intent(getApplicationContext(), DisplayerActivity.class);
                        intent.putExtra("Title", model.title);
                        intent.putExtra("Description", model.description);
                        intent.putExtra("Image URL", model.imageUrl);
                        startActivity(intent);
                    }
                }))
                .build();
        recyclerView.setAdapter(adapter);
    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadLink extends AsyncTask<Void, Void, Void> {

        private TitleViewModel mTitleTop7 = new TitleViewModel("Top7 Category", URL_TOP7);
        private TitleViewModel mTitleLast24 = new TitleViewModel("Last24 Category", URL_LAST24);
        private TitleViewModel mTitleAll = new TitleViewModel("All", URL_ALL);


        @Override
        protected Void doInBackground(final Void... voids) {

            rssItemTop7.clear();
            rssItemTop7.addAll(RssItem.getRssItems(URL_TOP7));
            rssItemLast24.clear();
            rssItemLast24.addAll(RssItem.getRssItems(URL_LAST24));
            rssItemAll.clear();
            rssItemAll.addAll(RssItem.getRssItems(URL_ALL));

            return null;
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            super.onPostExecute(aVoid);

            ArrayList<IViewModel> rssTypeItems = new ArrayList<>();

            rssTypeItems.add(mTitleTop7);

            for (int i = 0; i < 4; i++) {
                TextViewModel textViewModel = new TextViewModel(rssItemTop7.get(i).getTitle(),
                                                                rssItemTop7.get(i).getDescription(),
                                                                rssItemTop7.get(i).getImageURL());
                rssTypeItems.add(textViewModel);
            }

            rssTypeItems.add(mTitleLast24);

            for (int i = 0; i < 4; i++) {
                CardViewModel cardViewModel = new CardViewModel(rssItemLast24.get(i).getTitle(),
                                                                rssItemLast24.get(i)
                                                                             .getDescription(),
                                                                rssItemLast24.get(i).getImageURL(),
                                                                getApplicationContext());
                rssTypeItems.add(cardViewModel);
            }

            rssTypeItems.add(mTitleAll);

            for (int i = 0; i < 4; i++) {
                CardViewModel cardViewModel = new CardViewModel(rssItemAll.get(i).getTitle(),
                                                                rssItemAll.get(i).getDescription(),
                                                                rssItemAll.get(i).getImageURL(),
                                                                getApplicationContext());
                rssTypeItems.add(cardViewModel);
            }

            adapter.swapData(rssTypeItems);
        }
    }
}


